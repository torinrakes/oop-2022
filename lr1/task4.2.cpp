/**
 * Проведите компиляцию кода листинга 1 и проанализируйте выполнение программы инструментом Dr. Memory.
 * напишите исправленную версию программы.
 */

#include <iostream>
#include <memory>

using namespace std;

void fv() {
    int i = int(123);
    cout << "i = " << i << endl;
}

void fp1() {
    int *i = new int(123);
    cout << "*i = " << *i << endl;
    delete i;
}

void fp2() {
    int *i = new int(123);
    cout << "*i = " << *i << endl;
    delete i;
    i = new int(456);
    cout << "*i = " << *i << endl;
    delete i;
}

std::unique_ptr<int> fr() {
    std::unique_ptr<int> i = std::make_unique<int>(123);
    int *ip = i.get();
    cout << "i = " << *ip << endl;
    return i;
}

int main() {
    fv();
    fp1();
    fp2();
    std::unique_ptr<int> ifr = fr();
    cout << "*ifr = " << *ifr << endl;
    return 0;
}