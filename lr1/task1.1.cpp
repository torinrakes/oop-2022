/**
 * Написать программы, использующие функции потокового ввода-вывода
 * с полным указанием пространства имен std
 */

#include <iostream>

int main() {
    std::cout << "Hello, World!" << std::endl;
    return 0;
}

