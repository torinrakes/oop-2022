/**
 * Написать программы, использующие функции потокового ввода-вывода
 * с сокращенным указанием пространства имен (использовать using)
 */
#include <iostream>

using namespace std;

int main() {
    cout << "Hello, World!" << endl;
    return 0;
}
