/**
 * Написать процедуру, изменяющую значение своего аргумента, используя:
 * передачу параметра по значению;
 * передачу параметра по ссылке;
 * указатели;
 * Вывести на экран значение аргумента до и после вызова процедур. Поясните результат.
 * Чем отличается синтаксис вызова процедур?
 */

#include <iostream>

int incValue(int a) {
    a += 1;
    return a;
}

int incPointer(int *a) {
    *a += 1;
    return *a;
}

int incReference(int &a) {
    a += 1;
    return a;
}

int main() {
    int a = 0;
    std::cout << "Value = " << a << ", incValue = " << incValue(a) << ", value = " << a << std::endl;
    // a has not changed
    std::cout << "Value = " << a << ", incPointer = " << incPointer(&a) << ", value = " << a << std::endl;
    // a changed
    std::cout << "Value = " << a << ", incReference = " << incReference(a) << ", value = " << a << std::endl;
    // a changed
}
