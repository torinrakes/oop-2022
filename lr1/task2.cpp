/**
 * Написать программу, определяющую различные варианты функции plus:
 * обычное сложение и сложение по модулю 2.
 * Использовать механизм пространств имен
 */

#include <iostream>

namespace general {
    int plus(int x, int y) {
        return x + y;
    };
}

namespace mod2 {
    int plus(int x, int y) {
        return x ^ y;
    }
}

namespace mod2new {
    int plus(int x, int y) {
        return (x + y) % 2;
    }
}

int main() {
    std::cout << "General 2 + 2 = " << general::plus(2, 2) << std::endl;
    std::cout << std::showbase << std::hex << "Mod2 2 + 2 = " << mod2::plus(20, 4) << std::endl;
    std::cout << "Mod2new 2 + 2 = " << mod2new::plus(2, 2) << std::endl;
}
