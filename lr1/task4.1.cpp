/**
 * Проведите компиляцию кода листинга 1 и проанализируйте выполнение программы инструментом Dr. Memory.
 * объясните получаемые ошибки;
 */

#include <iostream>

using namespace std;

void fv() {
    int i = int(123);
    cout << "i = " << i << endl;
}

void fp1() {
    int *i = new int(123);
    cout << "*i = " << *i << endl;
    // delete not invoked => memory leak
}

void fp2() {
    int *i = new int(123);
    cout << "*i = " << *i << endl;
    i = new int(456);
    // reassigned pointer without deletion => memory leak
    cout << "*i = " << *i << endl;
    // delete not invoked => memory leak
}

int *fr() {
    int i = int(123);
    int *ip = &i;
    cout << "i = " << i << endl;
    return ip;
}

int main() {
    fv();
    fp1();
    fp2();
    int *ifr = fr();
    cout << "*ifr = " << *ifr << endl;
    // probably primitive-type destructors don't do anything
    return 0;
}