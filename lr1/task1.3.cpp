/**
 * Написать программы, использующие функции потокового ввода-вывода
 * вызывающую ошибку компиляции из-за отсутствия указания пространства имен
 */
#include <iostream>

int main() {
    // compile error
    cout << "Hello, World!" << endl;
    return 0;
}
