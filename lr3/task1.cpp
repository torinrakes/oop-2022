/**
 * Используя шаблоны реализовать функцию min, которая должна возвращать минимальный из двух элементов для любого сравнимого типа.
 */

#include <iostream>

template<class T>
T min(T a, T b) {
    return (a < b ? a : b);
}

int main() {
    std::cout << "Min of 1 and 2: " << min(1, 2) << std::endl;
    std::cout << "Min of 1.5 and 2.7: " << min(1.5, 2.7) << std::endl;
    return 0;
}