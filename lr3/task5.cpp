/**
 * Разработать класс Graph с использованием контейнеров из стандартной библиотеки шаблонов C++,
 * который бы представлял из себя реализацию ориентированного графа. Каждый узел должен быть представлен уникальным числом.
 * Разработанный класс должен содержать реализации следующих методов:
 * Graph::Graph(const vector &starts, const vector &ends) — строит ориентированный граф для переданных множеств вершин.
 * Элементы с одинаковым индексом в starts и ends представляют из себя направленное ребро графа.
 * int Graph::numOutgoing(const int nodeId) const — возвращает количество достижимых из вершины с nodeId вершин графа.
 * const vector<int> &Graph::adjacent(const int nodeId) const — возвращает ссылку
 * на список достижимых из вершины с nodeId вершин графа.
 * Все методы и конструкторы должны выбрасывать исключение invalid_argument в случае передачи в параметрах некорректных данных.
 * Проверить программу на предмет отсутствия утечек памяти.
 */

#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <set>

class Graph {
    std::unique_ptr<std::multimap<int, int>> data = std::make_unique<std::multimap<int, int>>();
public:
    Graph(const std::vector<int> &starts, const std::vector<int> &ends) {
        if (starts.size() != ends.size()) {
            throw std::invalid_argument("Vector lengths must be equal");
        }
        for (int i = 0; i < starts.size(); i++) {
            this->data->insert(std::pair<int, int>{starts.at(i), ends.at(i)});
        }
    }

    int numOutgoing(int node) {
        auto adjacent = this->adjacent(node);
        return adjacent.size();
    }

    std::vector<int> adjacent(int node) {
        std::set<int> neighbours = std::set<int>();
        std::set<int> candidates = std::set<int>{node};
        while (!candidates.empty()) {
            std::set<int> workSet = std::set<int>(candidates.begin(), candidates.end());
            for (auto work: workSet) {
                neighbours.insert(work);
                auto iterator = this->data->equal_range(work);
                for (auto it = iterator.first; it != iterator.second; it++) {
                    if (neighbours.count(it->second) == 0) {
                        candidates.insert(it->second);
                    }
                }
                candidates.erase(work);
            }
        }
        neighbours.erase(node);
        return std::vector<int>(neighbours.begin(), neighbours.end());
    };
};

int main() {
    std::vector<int> starts{1, 2, 2, 2, 3, 4, 5, 5, 7};
    std::vector<int> ends{2, 3, 4, 5, 5, 6, 1, 4, 5};
    std::unique_ptr<Graph> graph = std::make_unique<Graph>(starts, ends);
    std::cout << "NumOutgoing(3): " << graph->numOutgoing(3) << std::endl;
    for (auto i: graph->adjacent(3)) {
        std::cout << "Adjacent(3): " << i << std::endl;
    }
    return 0;
}
