/**
 * С использованием шаблонов разработать класс Stack, представляющий из себя реализацию структуры данных — стек.
 * Реализация класса должна содержать следующие методы (где тип T является параметризованным типом для класса Stack):
 * bool Stack::empty() — показывает пустой стек или нет.
 * void Stack::push(const T &item) — добавляет элемент в стек.
 * T &Stack::top() — возвращает ссылку на последний добавленный элемент.
 * void Stack::pop() — извлекает последний добавленный элемент из стека.
 * Проверить программу на предмет отсутствия утечек памяти.
 */

#include <iostream>

template<typename T>
class Collection {
    T **data;
public:
    explicit Collection(int n) {
        this->data = new T *[n];
    }

    virtual ~Collection() {
        delete[] this->data;
    }

    T *get(int n) {
        return this->data[n];
    }

    void set(int n, T &value) {
        this->data[n] = &value;
    }
};

template<typename T>
class Stack : public Collection<T> {
private:
    int capacity;
    int size;
public:
    explicit Stack(int n) : Collection<T>(n), capacity(n), size(0) {
    }

    ~Stack() override = default;

    void push(T &elem) {
        if (this->size < this->capacity) {
            this->set(this->size, elem);
            this->size += 1;
        } else {
            std::cout << "Stack is full" << std::endl;
        }
    }

    T &top() {
        if (this->size > 0) {
            return *this->get(this->size - 1);
        } else {
            std::cout << "Stack is empty" << std::endl;
        }
    }

    void pop() {
        if (this->size > 0) {
            this->size -= 1;
        } else {
            std::cout << "Stack is empty" << std::endl;
        }
    }

    bool empty() {
        return this->size == 0;
    }
};


int main() {
    auto *stack = new Stack<int>(10);
    std::cout << "Stack is empty: " << stack->empty() << std::endl;
    int *first = new int(1);
    int *second = new int(2);
    stack->push(*first);
    stack->push(*second);
    std::cout << "Stack top is: " << stack->top() << std::endl;
    stack->pop();
    std::cout << "Stack top is: " << stack->top() << std::endl;
    stack->pop();
    std::cout << "Stack is empty: " << stack->empty() << std::endl;
    delete first;
    delete second;
    delete stack;
    return 0;
}