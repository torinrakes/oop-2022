/**
 * Для класса Stack добавить дружественную (friend) функцию,
 * которая бы реализовывала операцию сложения с использованием оператора + для стека.
 * Поведение оператора должно быть таким, что при записи a + b получался бы новый стек,
 * содержащий сначала элементы стека a со следующими за ними элементами стека b.
 */

#include <iostream>

template<typename T>
class Collection {
    T **data;
public:
    explicit Collection(int n) {
        this->data = new T *[n];
    }

    virtual ~Collection() {
        delete[] this->data;
    }

    T *get(int n) {
        return this->data[n];
    }

    void set(int n, T &value) {
        this->data[n] = &value;
    }
};

template<typename T>
class Stack : public Collection<T> {
private:
    int capacity;
    int size;
public:
    explicit Stack(int n) : Collection<T>(n), capacity(n), size(0) {
    }

    int getCapacity() const {
        return capacity;
    }

    ~Stack() override = default;

    void push(T &elem) {
        if (this->size < this->capacity) {
            this->set(this->size, elem);
            this->size += 1;
        } else {
            std::cout << "Stack is full" << std::endl;
        }
    }

    T &top() {
        if (this->size > 0) {
            return *this->get(this->size - 1);
        } else {
            std::cout << "Stack is empty" << std::endl;
        }
    }

    void pop() {
        if (this->size > 0) {
            this->size -= 1;
        } else {
            std::cout << "Stack is empty" << std::endl;
        }
    }

    bool empty() {
        return this->size == 0;
    }
};

template<class T>
Stack<T> operator+(Stack<T> &first, Stack<T> &second) {
    auto reverseFirst = Stack<T>(first.getCapacity());
    while (!first.empty()) {
        reverseFirst.push(first.top());
        first.pop();
    }
    auto reverseSecond = Stack<T>(second.getCapacity());
    while (!second.empty()) {
        reverseSecond.push(second.top());
        second.pop();
    }
    auto newStack = Stack<T>(first.getCapacity() + second.getCapacity());
    while (!reverseFirst.empty()) {
        newStack.push(reverseFirst.top());
        first.push(reverseFirst.top());
        reverseFirst.pop();
    }
    while (!reverseSecond.empty()) {
        newStack.push(reverseSecond.top());
        second.push(reverseSecond.top());
        reverseSecond.pop();
    }
    return newStack;
}


int main() {
    int *first = new int(1);
    int *second = new int(2);
    auto *firstStack = new Stack<int>(10);
    firstStack->push(*first);
    firstStack->push(*second);
    auto *secondStack = new Stack<int>(10);
    secondStack->push(*second);
    secondStack->push(*first);
    auto resultStack = *firstStack + *secondStack;
    while (!resultStack.empty()) {
        std::cout << "Result stack top is: " << resultStack.top() << std::endl;
        resultStack.pop();
    }
    delete first;
    delete second;
    delete firstStack;
    delete secondStack;
    return 0;
}