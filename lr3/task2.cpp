/**
 * Реализовать функцию min из предыдущего задания, воспользовавшись макросом.
 */

#include <iostream>

#define min(a, b) (a < b ? a : b)

int main() {
    std::cout << "Min of 1 and 2: " << min(1, 2) << std::endl;
    std::cout << "Min of 1.5 and 2.7: " << min(1.5, 2.7) << std::endl;
    return 0;
}
