/**
 * Что будет выведено в результате выполнения программы
 */

#include <stdlib.h>
#include <iostream>

using namespace std;

class MyClass {
protected:
    int num;
public:
    MyClass(int num) : num(num) {
        cout << "MyClass constr" << endl;
    }

    virtual void print() {
        cout << "num = " << num << endl;
    }

    virtual ~MyClass() {
        cout << "MyClass destr" << endl;
    }
};

class Derived : public MyClass {
    int *inc;
public:
    Derived(int num, int inc) : MyClass(num) {
        cout << "Derived constr" << endl;
        this->inc = new int;
        *(this->inc) = inc;
    }

    void print() {
        cout << "num + inc = " << num + *inc << endl;
    }

    ~Derived() {
        cout << "Derived destr" << endl;
    }
};

class Container {
    MyClass *obj;
public:
    Container(MyClass *obj) : obj(obj) {}

    MyClass *getObj() {
        return obj;
    }
};

int main() {
    MyClass *obj = new Derived(1, 2);
    Container *container = new Container(obj);
    container->getObj()->print();
    delete container;
    delete obj;
    return 0;
}