/**
 * Что будет выведено в результате выполнения программы
 */

#include <iostream>

using namespace std;

struct A {
    A() { cout << "A"; }
};

struct B : public A {
    B() { cout << "B"; }
};

struct C : public A {
    C() { cout << "C"; }
};

struct D : public B, public C {
    D() { cout << "D"; }
};

int main() {
    auto d = D();
    return 0;
}