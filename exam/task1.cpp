/**
 * Пусть определена функция f
 * Что распечатает следующая программа
 */

#include <iostream>

void f(int &i) { i++; }

int main() {
    int x;
    x = 1;
    f(x);
    std::cout << x;
    return 0;
}