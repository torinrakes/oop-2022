/**
 * Используя модификации реализованных в предыдущих заданиях программ, исследовать зависимость объема памяти,
 * необходимой для выполнения программы, от используемого способа передачи параметров объектного типа:
 * по значению;
 * по ссылке;
 * использование указателя.
 */

#include <iostream>

class Expr {
public:
    virtual int eval() = 0;

    virtual ~Expr() {
        std::cout << "~Expr()" << std::endl;
    }
};

class Num : public Expr {
private:
    int a;
public:
    explicit Num(int a) : a(a) {}

    int eval() override {
        return a;
    }

    ~Num() override {
        std::cout << "~Num()" << std::endl;
    }
};

class Add : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Add(Expr &left, Expr &right) : left(&left), right(&right) {}

    Add(Expr *left, Expr *right) : left(left), right(right) {}

    int eval() override {
        return left->eval() + right->eval();
    }

    ~Add() override {
        std::cout << "~Add()" << std::endl;
    }

};

class Subtract : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Subtract(Expr &left, Expr &right) : left(&left), right(&right) {}

    Subtract(Expr *left, Expr *right) : left(left), right(right) {}

    int eval() override {
        return left->eval() - right->eval();
    }

    ~Subtract() override {
        std::cout << "~Subtract()" << std::endl;
    }
};

int main() {
    Num *one = new Num(1);
    Num *two = new Num(2);
    Num *three = new Num(3);
    Expr *add = new Add(*one, *two);
    Expr *subtract = new Subtract(*add, *three);
    std::cout << "Result is " << subtract->eval() << std::endl;
    delete subtract;
    delete add;
    delete three;
    delete two;
    delete one;
    return 0;
}
