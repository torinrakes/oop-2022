/**
 * Разработать систему классов, реализующих структуры структуры данных:
 *
 * Базовый класс: Collection(int n) — коллекция чисел, хранящая элементы в массиве длины n:
 * метод int get(int i) возвращает i-й элемент.
 *
 * Производные классы:
 *
 * Stack — стэк:
 * метод void put(int element) помещает элемент в конец стэка;
 * метод int take() выталкивает из стэка последний элемент.
 *
 * Queue — очередь:
 * метод void put(int element) помещает элемент в начало очереди;
 * метод int take() извлекает последний элемент очереди.
 *
 * Написать определения классов на языке C++. Написать следующую программу:
 * Создать массив коллекций.
 * Заполнить массив коллекциями различных типов (стэками и очередями).
 * В цикле: используя методы put и take провести запись и чтение значений.
 * В цикле: распечатать элементы коллекций массива.
 * Проверить программу на предмет отсутствия утечек памяти.
 */

#include <iostream>

template<typename T>
class Collection {
    T *data;
public:
    explicit Collection(int n) {
        this->data = new T[n];
    }

    virtual ~Collection() {
        delete[] this->data;
    }

    T get(int n) {
        return this->data[n];
    }

    void set(int n, T value) {
        this->data[n] = value;
    }

    virtual T take() = 0;

    virtual void put(T elem) = 0;
};

template<typename T>
class Stack : public Collection<T> {
private:
    int capacity;
    int size;
public:
    explicit Stack(int n) : Collection<T>(n), capacity(n), size(0) {
    }

    ~Stack() override = default;

    void put(T elem) override {
        if (this->size < this->capacity) {
            this->set(this->size, elem);
            this->size += 1;
        } else {
            std::cout << "Stack is full" << std::endl;
        }
    }

    T take() override {
        if (this->size > 0) {
            this->size -= 1;
            return this->get(this->size);
        } else {
            std::cout << "Stack is empty" << std::endl;
        }
    }
};

template<typename T>
class Queue : public Collection<T> {
private:
    int capacity;
    int current;
    int next;
public:
    explicit Queue(int n) : Collection<T>(n), capacity(n), current(-1), next(0) {
    }

    ~Queue() override = default;

    void put(T elem) override {
        if ((this->next - this->current) % this->capacity == 0) {
            std::cout << "Queue is full" << std::endl;
        } else {
            this->set(this->next, elem);
            if (this->current == -1) {
                this->current = this->next;
            }
            this->next = (this->next + 1) % this->capacity;
        }
    }

    T take() override {
        if (this->current == -1) {
            std::cout << "Queue is empty" << std::endl;
        } else {
            T value = this->get(this->current);
            if (((current + 1) % this->capacity) == next) {
                this->current = -1;
            } else {
                this->current = (current + 1) % this->capacity;
            }
            return value;
        }
    }
};

int main() {
    auto **array = new Collection<int> *[5];
    array[0] = new Stack<int>(10);
    array[1] = new Queue<int>(10);
    array[2] = new Stack<int>(10);
    array[3] = new Queue<int>(10);
    array[4] = new Stack<int>(10);

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 10; j++) {
            array[i]->put(j);
        }
    }

    for (int i = 0; i < 5; i++) {
        std::cout << "Elements in collection #" << i << ": ";
        for (int j = 0; j < 10; j++) {
            std::cout << array[i]->get(j) << ", ";
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < 5; i++) {
        delete array[i];
    }

    delete[] array;

    return 0;
}
