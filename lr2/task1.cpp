/**
 * Провести исследование порядка вызова конструкторов/деструкторов при наследовании:
 * составить программу, отображающую порядок выполнения операций при создании и удалении объекта.
 */

#include <iostream>

class A {
public:
    A() { std::cout << "A()" << std::endl; }

    virtual ~A() {
        std::cout << "~A()" << std::endl;
    }
};

class B : A {
public:
    B() { std::cout << "B()" << std::endl; }

    virtual ~B() {
        std::cout << "~B()" << std::endl;
    }
};

int main() {
    B();
    // A(), B(), ~B(), ~A()
    return 0;
}
