/**
 * Разработать систему классов для представления структуры синтаксического дерева арифметических выражений
 * и вычисления его значения:
 *
 * Базовый класс — Expr:
 * метод int eval() возвращает результат вычисления выражения.
 *
 * Производные классы:
 * Num(int n) — число.
 * Add(Expr* left, Expr* right) — сумма.
 * Subtract(Expr* left, Expr* right) — разность.
 */

#include <iostream>

class Expr {
public:
    virtual int eval() = 0;

    virtual ~Expr() {
        std::cout << "~Expr()" << std::endl;
    }
};

class Num : public Expr {
private:
    int a;
public:
    explicit Num(int a) : a(a) {}

    int eval() override {
        return a;
    }

    ~Num() override {
        std::cout << "~Num()" << std::endl;
    }
};

class Add : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Add(Expr *left, Expr *right) : left(left), right(right) {}

    int eval() override {
        return left->eval() + right->eval();
    }

    ~Add() override {
        std::cout << "~Add()" << std::endl;
    }

};

class Subtract : public Expr {
private:
    Expr *left;
    Expr *right;
public:
    Subtract(Expr *left, Expr *right) : left(left), right(right) {}

    int eval() override {
        return left->eval() - right->eval();
    }

    ~Subtract() override {
        std::cout << "~Subtract()" << std::endl;
    }
};


struct B {
};

struct C : public B {
    virtual ~C() = default;
};

int main() {
    Expr *one = new Num(1);
    Expr *two = new Num(2);
    Expr *three = new Num(3);
    Expr *onePlusTwo = new Add(one, two);
    Expr *onePlusTwoMinusThree = new Subtract(onePlusTwo, three);
    std::cout << "1 + 2 - 3 = " << onePlusTwoMinusThree->eval() << std::endl;
    delete one;
    delete two;
    delete three;
    delete onePlusTwo;
    delete onePlusTwoMinusThree;
    return 0;
}
